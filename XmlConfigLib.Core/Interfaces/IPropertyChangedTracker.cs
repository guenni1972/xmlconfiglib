﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 21-02-2015: Maarten Thomassen: Initial licenced file.
 * 13-09-2015: Maarten Thomassen: Added TrackedPropertyChanged event.
 */

using System.ComponentModel;

namespace XmlConfigLib.Core.Interfaces
{
    public interface IPropertyChangedTracker
    {
        #region Events

        event PropertyChangedEventHandler TrackedPropertyChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Specifies the size of the undo buffer i.e. how many times you can call <see cref="Undo()"/> on this class.
        /// Set to 0 for unlimited size buffer (will keep recording changes untill the program runs out of memory!).
        /// </summary>
        uint UndoDepth { get; set; }

        #endregion

        #region History Manipulation Methods

        /// <summary>
        /// Undo the last property change if there is one.
        /// </summary>
        void Undo();

        /// <summary>
        /// Redo the last undone property change if there is one.
        /// </summary>
        void Redo();

        /// <summary>
        /// Clear the entire property change history.
        /// </summary>
        void Clear();

        #endregion
    }
}