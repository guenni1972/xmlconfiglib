﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 24-01-2015: Maarten Thomassen: Initial licenced file.
 * 21-02-2015: Maarten Thomassen: Added IPropertyChangedChecker interface.
 * 13-09-2015: Maarten Thomassen: Added CheckedPropertyChanged event and implemented null propagation for event calling.
 */

using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using XmlConfigLib.Core.Annotations;
using XmlConfigLib.Core.Attributes;
using XmlConfigLib.Core.Extensions;
using XmlConfigLib.Core.Interfaces;

namespace XmlConfigLib.Core
{
    public abstract class PropertyChangedChecker : IPropertyChangedChecker, INotifyPropertyChanged
    {
        #region HasChanged Property
        private bool _hasChanges;

        [XmlIgnore]
        [DontCheckPropertyChanged]
        public bool HasChanges
        {
            get { return _hasChanges; }
            protected internal set
            {
                if (value.Equals(_hasChanges)) return;
                _hasChanges = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructor

        protected PropertyChangedChecker()
        {
            // Loop trough all properties and register to the PropertyChanged callback of properties that implement INotifyPropertyChanged
            foreach (var property in GetType().GetProperties().Where(x => x.PropertyType.GetInterfaces().Contains(typeof (INotifyPropertyChanged))))
            {
                RegisterINotifyPropertyChangedProperty(this, property);
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangedEventHandler CheckedPropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            // Check if the property is an INotifyPropertyChanged.
            if (propertyName != null)
            {
                var property = GetType().GetProperty(propertyName);

                // Don't set HasChanges to true or track the property if the property has the DontCheckPropertyChangedAttribute applied
                if (property != null && !property.HasAttribute<DontCheckPropertyChangedAttribute>())
                {
                    // Invoke the CheckedPropertyChanged handler
                    CheckedPropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

                    // Indicate the files has changes.
                    HasChanges = true;

                    // If the property implements INotifyPropertyChanged, register it.
                    if (property.Implements<INotifyPropertyChanged>())
                    {
                        RegisterINotifyPropertyChangedProperty(this, property);
                    }
                }
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Calback which gets called when any child property of any INotifyPropertyChanged property somehwere in the property tree of this class changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="propertyChangedEventArgs"></param>
        protected virtual void INotifyPropertyChangedOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var property = sender.GetType().GetProperty(propertyChangedEventArgs.PropertyName);
            
            // Don't set HasChanges to true or track the property if the property has the DontCheckPropertyChangedAttribute applied
            if (property == null || property.HasAttribute<DontCheckPropertyChangedAttribute>())
            {
                return;
            }

            // Indicate the files has changes.
            HasChanges = true;

            // If the property that was changed itself implements INotifyPropertyChanged, register it.
            if (property.Implements<INotifyPropertyChanged>())
            {
                RegisterINotifyPropertyChangedProperty(sender, property);
            }
        }

        private void RegisterINotifyPropertyChangedProperty(object parent, PropertyInfo property)
        {
            // Get the actual property
            var iNotifyPropertyChangedProperty = (INotifyPropertyChanged)property.GetValue(parent);

            if (iNotifyPropertyChangedProperty == null)
            {
                return;
            }

            // Register to the PropertyChanged callback.
            iNotifyPropertyChangedProperty.PropertyChanged += INotifyPropertyChangedOnPropertyChanged;

            // Loop trough all child properties and recursively register to the PropertyChanged callback of properties that implement INotifyPropertyChanged.
            foreach (var childProperty in property.PropertyType.GetProperties().Where(x => x.Implements<INotifyPropertyChanged>()))
            {
                RegisterINotifyPropertyChangedProperty(iNotifyPropertyChangedProperty, childProperty);
            }
        }

        #endregion
    }
}
