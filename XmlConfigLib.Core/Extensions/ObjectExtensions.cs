﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 16-02-2015: Maarten Thomassen: Initial licenced file.
 */

using System;
using System.ComponentModel;
using System.Linq;

namespace XmlConfigLib.Core.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Checks if an object implements a specific interface.
        /// </summary>
        /// <param name="obj">The object for which to check the interface.</param>
        /// <param name="TInterface">The interface to check for.</param>
        /// <returns>True if the object implements TInterface. False otherwise (or if TInterface is not an interface).</returns>
        public static bool Implements(this object obj, Type TInterface)
        {
            return TInterface.IsInterface && obj.GetType().Implements(TInterface);
        }

        /// <summary>
        /// Checks if an object implements a specific interface.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface to check for.</typeparam>
        /// <param name="obj">The object for which to check the interface.</param>
        /// <returns>True if the object implements TInterface. False otherwise (or if TInterface is not an interface).</returns>
        public static bool Implements<TInterface>(this object obj)
        {
            return obj.Implements(typeof(TInterface));
        }

        public static object GetPropertyValue(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj);
        }

        public static void SetPropertyValue(this object obj, string propertyName, object propertyValue)
        {
            obj.GetType().GetProperty(propertyName)?.SetValue(obj, propertyValue);
        }
    }
}
