﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 24-01-2015: Maarten Thomassen: Initial licenced file.
 * 16-02-2015: Maarten Thomassen: Added documentation and Implements() methods.
 */

using System;
using System.Linq;
using System.Reflection;

namespace XmlConfigLib.Core.Extensions
{
    public static class PropertyInfoExtensions
    {
        /// <summary>
        /// Gets an attribute declared on this propertyInfo's property of type TAttribute.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute to get.</typeparam>
        /// <param name="propertyInfo">The propertyInfo of the property for which to get the attribute.</param>
        /// <returns>The attribute of the specified type or null if there are multiple or 
        /// no attributes of the TAttribute declared on this property.</returns>
        public static TAttribute GetAttribute<TAttribute>(this PropertyInfo propertyInfo) 
            where TAttribute : Attribute
        {
            var attrs = propertyInfo.GetCustomAttributes<TAttribute>();
            return attrs.SingleOrDefault();
        }

        /// <summary>
        /// Checks if an attribute is declared on this propertyInfo's property of type TAttribute.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute to check.</typeparam>
        /// <param name="propertyInfo">The propertyInfo of the property for which to get the attribute.</param>
        /// <returns>True if one or more attributes of type TAttribute are declared on this property. False otherwise.</returns>
        public static bool HasAttribute<TAttribute>(this PropertyInfo propertyInfo) 
            where TAttribute : Attribute
        {
            var attrs = propertyInfo.GetCustomAttributes<TAttribute>();
            return attrs.Any();
        }

        /// <summary>
        /// Checks if a property implements a specific interface.
        /// </summary>
        /// <param name="propertyInfo">The propertyInfo of the property for which to check the interface.</param>
        /// <param name="TInterface">The interface to check for.</param>
        /// <returns>True if the property implements TInterface. False otherwise (or if TInterface is not an interface).</returns>
        public static bool Implements(this PropertyInfo propertyInfo, Type TInterface)
        {
            return TInterface.IsInterface && propertyInfo.PropertyType.Implements(TInterface);
        }

        /// <summary>
        /// Checks if a property implements a specific interface.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface to check for.</typeparam>
        /// <param name="propertyInfo"></param>
        /// <returns>True if the property implements TInterface. False otherwise.</returns>
        public static bool Implements<TInterface>(this PropertyInfo propertyInfo)
        {
            return propertyInfo.Implements(typeof(TInterface));
        }
    }
}
