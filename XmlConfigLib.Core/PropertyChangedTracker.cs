﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 24-01-2015: Maarten Thomassen: Initial licenced file.
 * 16-02-2015: Maarten Thomassen: Added Undo, Redo and Clear functionality.
 * 21-02-2015: Maarten Thomassen: Added IPropertyChangedTracker interface.
 * 27-02-2015: Maarten Thomassen: Fixed an issue with child properties of an Untracked INotifyPropertyChanged wrongfully getting added to history.
 * 05-09-2015: Maarten Thomassen: Fixed issue #3: https://bitbucket.org/MJLHThomassen/xmlconfiglib/issues/3/when-a-property-implementing
 * 13-09-2015: Maarten Thomassen: Added TrackedPropertyChanged event.
 */


using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using XmlConfigLib.Core.Annotations;
using XmlConfigLib.Core.Attributes;
using XmlConfigLib.Core.Extensions;
using XmlConfigLib.Core.Interfaces;

namespace XmlConfigLib.Core
{
    public abstract class PropertyChangedTracker : PropertyChangedChecker, IPropertyChangedTracker
    {
        private readonly Dictionary<object, Dictionary<string, List<object>>> _propertyHistory;
        private readonly Dictionary<object, Dictionary<string, List<object>>> _propertyFuture;
        private readonly ObservableCollection<KeyValuePair<object, string>> _globalHistory;
        private readonly ObservableCollection<KeyValuePair<object, string>> _globalFuture;

        private bool _undoing;
        private bool _redoing;

        private uint _undoDepth;

        #region Properties

        /// <summary>
        /// Specifies the size of the undo buffer i.e. how many times you can call <see cref="Undo()"/> on this class.
        /// Set to 0 for unlimited size buffer (will keep recording changes untill the program runs out of memory!).
        /// </summary>
        [XmlIgnore]
        [DontCheckPropertyChanged]
        public uint UndoDepth
        {
            get { return _undoDepth; }
            set
            {
                if (value == _undoDepth) return;
                _undoDepth = value;
                OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public ObservableCollection<KeyValuePair<object, string>> GlobalHistory => _globalHistory;

        [XmlIgnore]
        public ObservableCollection<KeyValuePair<object, string>> GlobalFuture => _globalFuture;

        #endregion

        #region Constructor

        protected PropertyChangedTracker() : base()
        {
            _propertyHistory = new Dictionary<object, Dictionary<string, List<object>>>(new ReferenceEqualityComparer<object>());
            _propertyFuture = new Dictionary<object, Dictionary<string, List<object>>>(new ReferenceEqualityComparer<object>());
            _globalHistory = new ObservableCollection<KeyValuePair<object, string>>();
            _globalFuture = new ObservableCollection<KeyValuePair<object, string>>();

            // Register all properties in the tracking dictionary
            InitializePropertyTimelineWithChildren(this);
        }

        /// <summary>
        /// Creates entries for a property and all child properties implementing INotifyPropertyChanged
        /// in the _propertyHistory and _propertyFuture lists.
        /// </summary>
        /// <param name="property">The property to initialize.</param>
        private void InitializePropertyTimelineWithChildren(object property)
        {
            if (property == null)
            {
                return;
            }

            // Create an entry for this class
            _propertyHistory.Add(property, new Dictionary<string, List<object>>());
            _propertyFuture.Add(property, new Dictionary<string, List<object>>());

            if(typeof(IEnumerable).IsAssignableFrom(property.GetType()) ||
                typeof(IEnumerable<>).IsAssignableFrom(property.GetType()))
            {
                return;
            }

            // Register all properties in the tracking dictionary
            foreach (var childProperty in property.GetType().GetProperties())
            {
                // Don't add properties which are annotated to not be tracked or don't have a setter
                if (childProperty.HasAttribute<DontTrackPropertyChangedAttribute>() ||
                    childProperty.HasAttribute<DontCheckPropertyChangedAttribute>() ||
                    !childProperty.CanWrite)
                {
                    continue;
                }

                var propertyValue = childProperty.GetValue(property);

                // Create an entry for this property
                _propertyHistory[property].Add(childProperty.Name, new List<object>());
                _propertyFuture[property].Add(childProperty.Name, new List<object>());

                // Add the current value to the property history
                _propertyHistory[property][childProperty.Name].Add(propertyValue);

                if (childProperty.Implements<INotifyPropertyChanged>())
                {
                    // Check to see if this property was already added in the past (see issue #3: https://bitbucket.org/MJLHThomassen/xmlconfiglib/issues/3/when-a-property-implementing)
                    if (!_propertyHistory.ContainsKey(propertyValue))
                    {
                        InitializePropertyTimelineWithChildren(propertyValue);
                    }
                }
            } 
        }

        /// <summary>
        /// Removes entries for a property and all child properties implementing INotifyPropertyChanged
        /// from the _propertyHistory and _propertyFuture lists.
        /// </summary>
        /// <param name="property">The property to remove.</param>
        private void RemovePropertyTimelineWithChildren(object property)
        {
            // Unregister all properties in the tracking dictionary
            foreach (var childProperty in property.GetType().GetProperties())
            {
                // Don't unregister properties which are not tracked.
                if (childProperty.HasAttribute<DontTrackPropertyChangedAttribute>() ||
                    childProperty.HasAttribute<DontCheckPropertyChangedAttribute>())
                {
                    continue;
                }

                var propertyValue = childProperty.GetValue(property);

                // Remove its childern if they are INotifyPropertyChanged first
                if (childProperty.Implements<INotifyPropertyChanged>())
                {
                    RemovePropertyTimelineWithChildren(propertyValue);
                }

                // Remove entry of this property
                _propertyHistory[property].Remove(childProperty.Name);
                _propertyFuture[property].Remove(childProperty.Name);
            }

            // Remove entry for this class
            _propertyHistory.Remove(property);
            _propertyFuture.Remove(property);
        }

        #endregion

        #region History Manipulation Methods

        /// <summary>
        /// Undo the last property change if there is one.
        /// </summary>
        public void Undo()
        {
            if (_globalHistory.Count == 0)
            {
                // Return if there is nothing to undo
                return;
            }

            _undoing = true;

            // Get the last history entry
            var lastEntry = _globalHistory.Last();

            // Get the history entries parent property and property name
            var parent = lastEntry.Key;
            var propertyName = lastEntry.Value;

            // Get the property history from the property history dictionary
            var propertyHistory = _propertyHistory[parent][propertyName];

            // Get the current value.
            var currentValue = propertyHistory.Last();

            // Save the future of the property
            _propertyFuture[parent][propertyName].Insert(0, currentValue);

            // Remove the last value from the history.
            propertyHistory.RemoveAt(propertyHistory.Count - 1);

            // Get the previous value.
            var previousValue = propertyHistory.Last();

            // Set the value of the property back to its previous value.
            parent.SetPropertyValue(propertyName, previousValue);

            // Save the global future and remove the undid item from history
            _globalFuture.Insert(0, lastEntry);
            _globalHistory.RemoveAt(_globalHistory.Count - 1);

            _undoing = false;
        }

        /// <summary>
        /// Redo the last undone property change if there is one.
        /// </summary>
        public void Redo()
        {
            // Return if there is nothing to redo
            if (_globalFuture.Count == 0)
            {
                return;
            }

            _redoing = true;

            // Get the first future entry
            var nextEntry = _globalFuture.First();

            // Get the future entries parent property and property name
            var parent = nextEntry.Key;
            var propertyName = nextEntry.Value;

            // Get the property future from the property future dictionary
            var propertyFuture = _propertyFuture[parent][propertyName];

            // Get the next property value from its future
            var nextValue = propertyFuture.First();

            // Set the value of the property back to its next value.
            parent.SetPropertyValue(propertyName, nextValue);

            // Re-add the current value to the property history
            _propertyHistory[parent][propertyName].Add(nextValue);

            // Remove this future of the property
            propertyFuture.RemoveAt(0);

            // Save the global history and remove the undid item from future
            _globalHistory.Add(nextEntry);
            _globalFuture.RemoveAt(0);

            _redoing = false;
        }

        /// <summary>
        /// Clear the entire property change history.
        /// </summary>
        public void Clear()
        {
            // Remove properties from the global history one by one.
            // This also removes their respective property futures.
            while (_globalHistory.Any())
            {
                RemoveOldestGlobalHistoryEntry();
            }

            // Clear the global futures aswell.
            _globalFuture.Clear();
            
        }

        /// <summary>
        /// Removes the oldest (first) entry from the _globalHistory list together with
        /// all accompanying property history and future entries.
        /// </summary>
        private void RemoveOldestGlobalHistoryEntry()
        {
            // Get the first entry of the history
            var firstEntry = _globalHistory.First();

            // Get the property parent, name and current value
            var historyPropertyParent = firstEntry.Key;
            var historyPropertyName = firstEntry.Value;
            var historyProperty = historyPropertyParent.GetPropertyValue(historyPropertyName);

            // Remove the children of the property
            if (historyProperty.Implements<INotifyPropertyChanged>())
            {
                RemovePropertyTimelineWithChildren(historyProperty);
            }

            // Remove the property from its own parent
            _propertyHistory[historyPropertyParent][historyPropertyName].RemoveAt(0);

            // Remove the global history entry
            _globalHistory.RemoveAt(0);
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler TrackedPropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName != null)
            {
                var property = GetType().GetProperty(propertyName);

                if (property != null && 
                    !property.HasAttribute<DontTrackPropertyChangedAttribute>() &&
                    !property.HasAttribute<DontCheckPropertyChangedAttribute>())
                {
                    TrackedPropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }

                AddChangeToHistory(this, property);
            }

            base.OnPropertyChanged(propertyName);
        }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Calback which gets called when any child property of any INotifyPropertyChanged property somehwere in the property tree of this class changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="propertyChangedEventArgs"></param>
        protected override void INotifyPropertyChangedOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            base.INotifyPropertyChangedOnPropertyChanged(sender, propertyChangedEventArgs);

            var property = sender.GetType().GetProperty(propertyChangedEventArgs.PropertyName);

            // If the property is a child of a class instance being tracked, add the change to the history
            if (property != null && _propertyHistory.ContainsKey(sender))
            {
                AddChangeToHistory(sender, property);
            }
        }

        private void AddChangeToHistory(object parent, PropertyInfo property)
        {
            if (property.HasAttribute<DontTrackPropertyChangedAttribute>() ||
                property.HasAttribute<DontCheckPropertyChangedAttribute>())
            {
                return;
            }

            // Remove the first entry if the UndoDepth has been reached
            if (UndoDepth > 0 && _globalHistory.Count == UndoDepth + 1)
            {
                RemoveOldestGlobalHistoryEntry();
            }

            // Only log changes if we are not undoing or redoing
            if (_undoing || _redoing)
            {
                return;
            }

            // Empty the global future if we made a change not by re-doing
            _globalFuture.Clear();
            foreach (var parentKey in _propertyFuture)
            {
                foreach (var futureKey in parentKey.Value)
                {
                    futureKey.Value.Clear();
                }
            }

            // Log the change in the global history
            _globalHistory.Add(new KeyValuePair<object, string>(parent, property.Name));

            var propertyValue = property.GetValue(parent);

            var propertyHistory = _propertyHistory[parent][property.Name];

            // If the property being added is an INotifyPropertyChanged, Initialize a dictionary entry for its children.
            if (property.Implements<INotifyPropertyChanged>())
            {
                // Check to see if this property was already added in the past (see issue #3: https://bitbucket.org/MJLHThomassen/xmlconfiglib/issues/3/when-a-property-implementing)
                if (!_propertyHistory.ContainsKey(propertyValue))
                {
                    InitializePropertyTimelineWithChildren(propertyValue);
                }
            }

            propertyHistory.Add(propertyValue);
        }

        #endregion
    }
}
