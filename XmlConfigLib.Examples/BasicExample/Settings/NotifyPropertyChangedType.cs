﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 18-01-2014: Maarten Thomassen: Initial licenced file.
 */

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using XmlConfigLib.Core.Annotations;

namespace XmlConfigLib.Examples.BasicExample.Settings
{
    public class NotifyPropertyChangedType : INotifyPropertyChanged
    {
        private Guid _guidProperty;
        private string _stringProperty;
        private NestedNotifyPropertyChangedType _nestedNotifyPropertyChangedType = new NestedNotifyPropertyChangedType();

        public Guid GuidProperty
        {
            get { return _guidProperty; }
            set
            {
                if (value.Equals(_guidProperty)) return;
                _guidProperty = value;
                OnPropertyChanged();
            }
        }

        public string StringProperty
        {
            get { return _stringProperty; }
            set
            {
                if (value == _stringProperty) return;
                _stringProperty = value;
                OnPropertyChanged();
            }
        }

        public NestedNotifyPropertyChangedType NestedNotifyPropertyChangedType
        {
            get { return _nestedNotifyPropertyChangedType; }
            set
            {
                if (Equals(value, _nestedNotifyPropertyChangedType)) return;
                _nestedNotifyPropertyChangedType = value;
                OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}