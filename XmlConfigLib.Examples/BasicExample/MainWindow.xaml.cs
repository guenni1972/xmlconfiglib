﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 18-01-2014: Maarten Thomassen: Initial licenced file.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using XmlConfigLib.Core;
using XmlConfigLib.Core.Annotations;
using XmlConfigLib.Core.Extensions;
using XmlConfigLib.Examples.BasicExample.Settings;

namespace XmlConfigLib.Examples.BasicExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private BasicExampleSettings _settings;

        #region Properties

        public ObservableCollection<string> History { get; set; } = new ObservableCollection<string>();

        public ObservableCollection<string> Future { get; set; } = new ObservableCollection<string>();

        public BasicExampleSettings Settings
        {
            get { return _settings; }
            set
            {
                if (Equals(value, _settings)) return;
                _settings = value;

                // In order to support bindings properly, the DataContext needs to implement INotifyPropertyChanged
                OnPropertyChanged(); 
            }
        }
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            try
            {
                Settings = XmlHelper.LoadXmlFile<BasicExampleSettings>("settings.xml");
                Settings.Clear();
            }
            catch (FileNotFoundException)
            {
                Settings = new BasicExampleSettings();
            }

            Settings.PropertyChanged += SettingsOnPropertyChanged;

            Settings.GlobalHistory.CollectionChanged += GlobalHistoryOnCollectionChanged;
            Settings.GlobalFuture.CollectionChanged += GlobalFutureOnCollectionChanged;

            foreach (var item in Settings.GlobalHistory)
            {
                History.Add(item.Value + ": " + item.Key.GetPropertyValue(item.Value));
            }
            foreach (var item in Settings.GlobalFuture)
            {
                Future.Add(item.Value + ": " + item.Key.GetPropertyValue(item.Value));
            }
        }

        private void SettingsOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Settings.HasChanges)
            {
                XmlHelper.SaveXmlFile(Settings, "settings.xml");
            }          
        }

        private void GlobalHistoryOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems.Cast<KeyValuePair<object, string>>())
                    {
                        History.Add(item.Value + ": " + item.Key.GetPropertyValue(item.Value));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    History.RemoveAt(args.OldStartingIndex);
                    break;
            }
        }

        private void GlobalFutureOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems.Cast<KeyValuePair<object, string>>())
                    {
                        Future.Add(item.Value + ": " + item.Key.GetPropertyValue(item.Value));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    Future.RemoveAt(args.OldStartingIndex);
                    break;
            }
        }


        private void ChangeGuidClick(object sender, RoutedEventArgs e)
        {
            Settings.NotifyPropertyChangedType.GuidProperty = Guid.NewGuid();
        }

        private void ChangeNotGuidClick(object sender, RoutedEventArgs e)
        {
            Settings.NotNotifyPropertyChangedType.GuidProperty = Guid.NewGuid();
        }

        Random rand = new Random();
        private void NewNestedPropertyChangedType(object sender, RoutedEventArgs e)
        {
            var buf = new byte[16];
            rand.NextBytes(buf);
            Settings.NotifyPropertyChangedType.NestedNotifyPropertyChangedType = new NestedNotifyPropertyChangedType()
            {
                StringProperty = Convert.ToBase64String(buf)
            };
        }

        private void UndoButton_OnClick(object sender, RoutedEventArgs e)
        {
            _settings.Undo();
        }

        private void RedoButton_OnClick(object sender, RoutedEventArgs e)
        {
            _settings.Redo();
        }

        private void ClearButton_OnClickButton_OnClick(object sender, RoutedEventArgs e)
        {
            _settings.Clear();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
