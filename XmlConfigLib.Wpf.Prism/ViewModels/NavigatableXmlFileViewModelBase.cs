﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 17-01-2014: Maurice Camp, Maarten Thomassen: Initial licenced file.
 * 12-09-2015: Maarten Thomassen: Added constructor with parameter.
 */

using System;
using System.Windows;
using Microsoft.Practices.Prism.Regions;
using XmlConfigLib.Core;
using XmlConfigLib.Wpf.Resources;
using XmlConfigLib.Wpf.ViewModels;

namespace XmlConfigLib.Wpf.Prism.ViewModels
{
    public abstract class NavigatableXmlFileViewModelBase<TXmlFile> : XmlFileViewModelBase<TXmlFile>, IConfirmNavigationRequest
        where TXmlFile : XmlFileBase, new()
    {

        #region Constructor

        protected NavigatableXmlFileViewModelBase(TXmlFile settingsFile) : base(settingsFile)
        {

        }

        #endregion

        #region Abstract Methods

        protected abstract void OnNavigatedTo();

        protected abstract void OnNavigatedFrom();

        #endregion

        #region IConfirmNavigationRequest

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var loadConfig = (string)navigationContext.Parameters["DefaultConfiguration"];
            if (loadConfig != null)
            {
                // LoadConfig configuration
                LoadConfig(loadConfig);
            }
            else
            {
                SettingsFile = new TXmlFile();
            }

            OnNavigatedTo();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            OnNavigatedFrom();

            if (!string.IsNullOrEmpty(CurrentlyLoadedSettingsFile))
                navigationContext.Parameters.Add("DefaultConfiguration", CurrentlyLoadedSettingsFile);
        }

        public void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
        {
            // Check if the settings file has changes
            // Check if settings are saved, if not, prompt to save
            if (!SettingsFile.HasChanges)
            {
                continuationCallback(true);
                return;
            }

            var result = MessageBox.Show(XmlFileViewResources.MessageBox_UnsavedChanges_Message,
                XmlFileViewResources.MessageBox_UnsavedChanges_Title, MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.No || result == MessageBoxResult.Cancel)
            {
                continuationCallback(result == MessageBoxResult.No);
                return;
            }

            if (string.IsNullOrEmpty(CurrentlyLoadedSettingsFile))
            {
                if (SaveConfigWithDialog())
                {
                    continuationCallback(false);
                    return;
                }
            }
            else
                SaveConfig(CurrentlyLoadedSettingsFile);

            continuationCallback(true);
        }

        #endregion
    }
}

